package com.example.videoplayerdemo.network;

import com.example.videoplayerdemo.models.PlayerModel;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface MyWebService {
    String BASE_URL="https://www.example.com/services/rest/";

    Retrofit retrofit=new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();


    @GET(".")
    Call<ArrayList<PlayerModel>> getMediaList(@QueryMap Map<String, Object> queries);


}
