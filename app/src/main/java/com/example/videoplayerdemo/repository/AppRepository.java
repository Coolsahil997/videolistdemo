package com.example.videoplayerdemo.repository;

import android.content.Context;
import android.util.Log;

import com.example.videoplayerdemo.models.PlayerModel;
import com.example.videoplayerdemo.network.MyWebService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppRepository {
    private static AppRepository ourInstance;
    private Executor mExecutor = Executors.newSingleThreadExecutor();
    ArrayList<PlayerModel> mVideoArrayList;

    public static AppRepository getInstance(Context context) {
        return ourInstance = new AppRepository();
    }


    public ArrayList<PlayerModel> getVideos(String search_text, String page) {
        MyWebService webService = MyWebService.retrofit.create(MyWebService.class);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("search_text",search_text);
        hashMap.put("page",page);
        Call<ArrayList<PlayerModel>> call = webService.getMediaList(hashMap);

        call.enqueue(new Callback<ArrayList<PlayerModel>>() {
            @Override
            public void onResponse(Call<ArrayList<PlayerModel>> call, final Response<ArrayList<PlayerModel>> response) {
                if (response.isSuccessful()) {
                    mVideoArrayList = response.body();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PlayerModel>> call, Throwable t) {
                Log.e("failure", t.getMessage());
                mVideoArrayList = null;
            }
        });

        return mVideoArrayList;
    }

}
