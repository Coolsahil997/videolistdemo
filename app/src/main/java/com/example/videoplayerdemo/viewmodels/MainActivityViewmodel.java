package com.example.videoplayerdemo.viewmodels;

import android.app.Application;


import com.example.videoplayerdemo.models.PlayerModel;
import com.example.videoplayerdemo.repository.AppRepository;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

public class MainActivityViewmodel
        extends AndroidViewModel {

    private AppRepository mAppRepository;

    public MainActivityViewmodel(@NonNull Application application) {
        super(application);
        mAppRepository = AppRepository.getInstance(application.getApplicationContext());
    }

    public ArrayList<PlayerModel> getVideos(String search_text,String page) {
         return mAppRepository.getVideos(search_text,page);
    }

}
